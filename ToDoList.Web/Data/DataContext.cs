﻿using ToDoList.Web.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace ToDoList.Web.Data 
{
    public class DataContext : IdentityDbContext<User, IdentityRole<Guid>, Guid>
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<ToDoItem> List { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<IdentityRole<Guid>>().HasData(new List<IdentityRole<Guid>> {
                new IdentityRole<Guid>()
                {
                    Id = Guid.NewGuid(),
                    Name = "admin",
                    NormalizedName = "ADMIN"
                },
                new IdentityRole<Guid>()
                {
                    Id = Guid.NewGuid(),
                    Name = "user",
                    NormalizedName = "USER"
                }
            });
            builder.Entity<User>().HasMany<ToDoItem>()
                .WithOne(x => x.User).HasForeignKey(x => x.UserId);
            builder.Entity<ToDoItem>().HasKey(x => x.Id);
        }
    }
}
