﻿using Microsoft.IdentityModel.Tokens;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace ToDoList.Web
{
    public static class AuthOptions
    {
        public const string ISSUER = "MyAuthServer"; // издатель токена
        public const string AUDIENCE = "MyAuthClient"; // потребитель токена
        const string KEY = "mysupersecret_secretkey!123";   // ключ для шифрации
        public const int LIFETIME = 30; // время жизни токена - 30 минут

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }

        public static X509SecurityKey GetCertificate()
        {
            X509Certificate2 x509Certificate2 = new X509Certificate2("Sertificates/cert_key.p12", "1234"); //add path
            X509SecurityKey x509SecurityKey = new X509SecurityKey(x509Certificate2);
            return x509SecurityKey;
        }
    }
}
