﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDoList.Web.Data;
using ToDoList.Web.Models;
using ToDoList.Web.ViewModels;

namespace ToDoList.Web.Services 
{
    public interface IToDoService 
    {
        Task<List<ToDoItemViewModel>> GetToDoItems(Guid guid);
        void AddItem(ToDoItemViewModel item, Guid guid);
        void UpdateItem(ToDoItemViewModel item);
        void DeleteItem(int id);
    }

    public class ToDoService : IToDoService 
    {
        private readonly DbSet<ToDoItem> _toDoItems;
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;

        public ToDoService (IDbContextFactory<DataContext> contextFactory, IMapper mapper, UserManager<User> userManager)
        {
            _mapper = mapper;
            _userManager = userManager;
            _context = contextFactory.CreateDbContext();
            _toDoItems = _context.Set<ToDoItem>();
        }

        public async Task<List<ToDoItemViewModel>> GetToDoItems(Guid guid)
        {
            User user = await _userManager.FindByIdAsync(guid.ToString());

            if ((await _userManager.GetClaimsAsync(user)).Any(x => x.Value == "admin"))
            {
                return _mapper.Map<List<ToDoItemViewModel>>(_toDoItems.ToList());
            } else
            {
                return _mapper.Map<List<ToDoItemViewModel>>(_toDoItems.Where(element => element.UserId == guid).ToList());
            }
        }

        public void AddItem(ToDoItemViewModel item, Guid userId)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<ToDoItemViewModel, ToDoItem>()
                .ForMember("UserId", opt => opt.MapFrom(src => userId)));

            ToDoItem element = new Mapper(config).Map<ToDoItemViewModel, ToDoItem>(item);
            _toDoItems.Add(element);
            _context.SaveChanges();
        }

        public void UpdateItem(ToDoItemViewModel item)
        {
            ToDoItem element = _mapper.Map<ToDoItem>(item);
            _toDoItems.Update(element);
            _context.SaveChanges();
        }

        public void DeleteItem(int id)
        {
            var element = _toDoItems.FirstOrDefault(e => e.Id == id);
            if(element == null)
            {
                return;
            }
            _toDoItems.Remove(element);
            _context.SaveChanges();
        }
    }
}