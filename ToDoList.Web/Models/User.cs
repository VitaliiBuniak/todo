﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace ToDoList.Web.Models
{
    public class User : IdentityUser<Guid>
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public List<ToDoItem> Items { get; set; }
    }
}
