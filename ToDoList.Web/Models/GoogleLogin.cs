﻿
namespace ToDoList.Web.Models
{
    public class GoogleLogin
    {
        public string provider { set; get; }

        public string returnUrl { set; get; }
    }
}
