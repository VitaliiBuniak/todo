﻿using AutoMapper;
using ToDoList.Web.ViewModels;

namespace ToDoList.Web.Models
{
    public class ToDoConfiguration : Profile
    {
        public ToDoConfiguration()
        {
            CreateMap<ToDoItemViewModel, ToDoItem>();
            CreateMap<ToDoItem, ToDoItemViewModel>();
        }
    }
}
