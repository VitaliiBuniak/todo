﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace ToDoList.Web.Models
{
    public class RegisterModel
    {
        [JsonPropertyName("username")]
        public string Username { set; get; }

        [JsonPropertyName("password")]
        public string Password { set; get; }

        [JsonPropertyName("firstname")]
        public string FirstName { set; get; }

        [JsonPropertyName("lastname")]
        public string LastName { set; get; }

        [Required]
        [EmailAddress]
        [JsonPropertyName("email")]
        public string Email { set; get; }
    }
}
