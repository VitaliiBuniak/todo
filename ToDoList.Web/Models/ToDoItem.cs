﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ToDoList.Web.Models 
{
    public class ToDoItem
    {
        [Key]
        public int Id { set; get; }

        public Guid UserId { get; set; }
        public User User { get; set; }

        [MaxLength(200)]
        public string Description { set; get; }

        public string Deadline { set; get; }

        public bool Status { set; get; }
    }
}
