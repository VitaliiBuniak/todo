﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using ToDoList.Web.Models;

namespace ToDoList.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegController : ControllerBase
    {
        private readonly UserManager<User> _userManager;

        public RegController(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        [HttpPost]
        public async Task<IActionResult> Register ([FromBody] RegisterModel registerdata)
        {
            if (await _userManager.FindByEmailAsync(registerdata.Email) == null)
            {
                await _userManager.CreateAsync(new User //add incorrect password validation and response
                {
                    UserName = registerdata.Username,
                    FirstName = registerdata.FirstName,
                    LastName = registerdata.LastName,
                    Email = registerdata.Email

                }, registerdata.Password);
                User user = await _userManager.FindByEmailAsync(registerdata.Email);
                await _userManager.AddClaimsAsync(user , new List<Claim>
                {
                    new Claim(ClaimTypes.Sid, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, "user")
                });
                return Ok();
            }
            return BadRequest(new { errorText = "User is already registered" });
        }
    }
}
