﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using ToDoList.Web.ViewModels;
using ToDoList.Web.Services;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace ToDoList.Web.Controllers 
{
    [Route("api/[controller]")]
    [ApiController]
    public class ToDoController : ControllerBase 
    {
        private readonly IToDoService _toDoService;

        public ToDoController(IToDoService toDoService) 
        {
            _toDoService = toDoService;
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<ActionResult<List<ToDoItemViewModel>>> GetAdminItems()
        {
            return Ok(await _toDoService.GetToDoItems(Guid.Parse(HttpContext.User.FindFirstValue(ClaimTypes.Sid))));
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public void Add([FromBody] ToDoItemViewModel json)
        {
            _toDoService.AddItem(json, Guid.Parse(HttpContext.User.FindFirstValue(ClaimTypes.Sid)));
        }

        [HttpPut("{id:int}")]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public IActionResult Update([FromBody] ToDoItemViewModel json)
        {
            _toDoService.UpdateItem(json);
            return NoContent();
        }

        [HttpDelete("{id:int}")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = "RequireAdministratorRole")] // Roles = "admin"
        public IActionResult Delete(int id)
        {
            _toDoService.DeleteItem(id);
            return NoContent();
        }
    }
}