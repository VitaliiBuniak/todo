﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using ToDoList.Web.Models;

namespace ToDoList.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly UserManager<User> _userManager;

        private readonly SignInManager<User> _signInManager;

        public AuthController(UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody] LogInViewModel logindata)
        {
            User user = await _userManager.FindByEmailAsync(logindata.Email);
            var identityResult = await _signInManager.PasswordSignInAsync(user.UserName, logindata.Password, true, false);
            
            if (!identityResult.Succeeded)
            {
                return BadRequest(new { errorText = "Invalid username or password." });
            }

            using X509Certificate2 x509Certificate2 = new X509Certificate2("Sertificates/cert_key.p12", "1234"); //add path
            X509SecurityKey x509SecurityKey = new X509SecurityKey(x509Certificate2);

            var jwt = new JwtSecurityToken(
                issuer: AuthOptions.ISSUER,
                audience: AuthOptions.AUDIENCE,
                notBefore: DateTime.UtcNow,
                claims: await _userManager.GetClaimsAsync(user),
                expires: DateTime.UtcNow.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                signingCredentials: new SigningCredentials(x509SecurityKey, SecurityAlgorithms.RsaSha256Signature)
                //signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256)
                );
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var response = new
            {
                access_token = encodedJwt,
                username = user.FirstName
            };

            return Ok(response);
        }
    }
}
