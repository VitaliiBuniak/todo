﻿using System;
using System.Text.Json.Serialization;

namespace ToDoList.Web.ViewModels
{
    public class ToDoItemViewModel
    {
        [JsonPropertyName("guid")]
        public Guid UserID { get; set; }

        [JsonPropertyName("id")]
        public int Id { set; get; }

        [JsonPropertyName("description")]
        public string Description { set; get; }

        [JsonPropertyName("deadline")]
        public string Deadline { set; get; }

        [JsonPropertyName("status")]
        public bool Status { set; get; }
    }
}